#!/usr/bin/python3
#
# Name: sns-zoom-ip.py
# Description: Retrieves all IPv4 addresses from https://support.zoom.us/hc/en-us/articles/201362683-Network-firewall-or-proxy-server-settings-for-Zoom
# And outputs an CSV file that can be imported into the STormshield Network
# Security appliance.
# Created by: Luke "sludge3000" Michael Savage
#

# Import required modules
import urllib.request
import re
import ipaddress


# Read page bytes and convert to UTF8
page = urllib.request.urlopen("https://support.zoom.us/hc/en-us/articles/201362683-Network-firewall-or-proxy-server-settings-for-Zoom")
pagebytes = page.read()
pagestr = pagebytes.decode("UTF8")
page.close()


# RegEx magic
# IPv4 networks with CIDR mask
ipv4networklist = re.findall(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,3})',
pagestr)


# Sort and order
uniqipv4nets = set(ipv4networklist)
orderipv4nets = sorted(uniqipv4nets)


ipv4group = ''

with open('IMPORT_Zoom.csv', 'w+', newline='') as outfile:
    counteripv4nets = 0
    for ip in orderipv4nets:
        counteripv4nets += 1
        network = ipaddress.ip_network(ip).network_address
        netmask = ipaddress.ip_network(ip).netmask
        prefixlen = ipaddress.ip_network(ip).prefixlen
        outfile.write("network,ZoomNet_" + str(counteripv4nets) + ',' + str(network) + ',' + str(netmask) + ',' + str(prefixlen) + ",,," + '""\n')
        ipv4group = ipv4group + 'ZoomIPv4Net_' + str(counteripv4nets) + ','
    outfile.write('group,Grp_Zoom_IPv4,"' + ipv4group + '",""\n')

