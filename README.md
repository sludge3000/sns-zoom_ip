# SNS-ZOOM_IP

Python script to retrieve IP addresses and URLs from https://support.zoom.us/hc/en-us/articles/201362683-Network-firewall-or-proxy-server-settings-for-Zoom and output a formatted CSV file to be imported into the SNS.